import { Url } from 'url';

export interface Component {
  id: number;
  volume: number;
  name: string;
  componentTypeName: ComponentType;
  unit: Unit;
  link: Url;
  description: string;
}

export type ComponentType = 'soda' | 'alcohol' | 'other';

export interface Unit {
  unitName: string;
  unitDescription: string;
}
