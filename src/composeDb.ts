import * as fs from 'fs';
import { Drink } from './interfaces/drink';

const FILEDIR = __dirname;

const tab = ['Maciek', 'kocha', 'Ele'];

console.log(tab);

const a = 2;
let b = 3;

function multiply(x, y) {
  const result = x * y;
  return result;
}

const c = multiply(a, b);
const d = {
  a: 1,
  b: 2
};

d;

// compose().then(val => console.log(val));

function compose() {
  return new Promise((resolve, reject) => {
    fs.readFile(`${__dirname}/dbfiles/drinks.json`, 'utf-8', (err, data) => {
      if (err) {
        console.log(err);
      }
      const drinks: Drink[] = JSON.parse(data);
      const drinksToSave = drinks.map(drink => {
        return {
          ...drink,
          component: drink.components.map(component => {
            return { id: component.id, volume: component.volume };
          })
        };
      });
      resolve(drinksToSave);
    });
  });
}
