import * as fr from 'fs';
import * as readline from 'readline';

function readName() {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('What is your name?', function(answer) {
    console.log('UR answer is: ', answer);
    rl.close();
  });
}

function greeter(person) {
  return 'Hello, ' + person;
}

let user = 'Jane User';

readName();
