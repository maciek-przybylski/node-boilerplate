module.exports = function() {
  return {
    files: ['src/**/*.ts', '!src/**/*spec.ts'],
    tests: ['src/**/*spec.ts'],
    setup: function() {
      global.expect = require('chai').expect;
    },
    env: { type: 'node', runner: 'node' }
  };
};
