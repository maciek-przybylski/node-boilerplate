import fetch from 'node-fetch';
import * as fs from 'fs';
import * as patch from 'path';

import { Drink } from './interfaces/drink';

export class DrinkService {
  drinkArray: Array<Drink>;
  urlBase = `http://jakidrink.pl`;
  constructor() {
    this.drinkArray = [];
  }

  public getAll(): Promise<Drink[]> {
    return new Promise((resolve, reject) => {
      const array: Array<Drink> = [];
      let index = 1;
      this.getDrinkName(index).then((drink: Drink | null) => {
        if (drink) {
          array.push(drink);
          index++;
        } else {
          resolve(array);
        }
      });
    });
  }
  private async getDrinkName(index: number): Promise<Drink | null> {
    let response = await fetch(`${this.urlBase}/drink/get/${index}`);
    if (response.ok) {
      return await response.json();
    }
    return null;
  }

  private appendToAray(drink: Drink) {
    this.drinkArray.push(drink);
  }

  private async writeToFile(fileName: string) {
    if (this.drinkArray.length === 0) {
      return;
    }
    return fs.writeFile(
      patch.resolve(__dirname, fileName),
      JSON.stringify(this.drinkArray, null, '  '),
      function(err) {
        if (err) return console.log('oups:', err);
      }
    );
  }
}
