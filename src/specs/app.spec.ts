import chai = require('chai');
import { DrinkService } from './../app';
import * as nock from 'nock';
import { Drink } from '../interfaces/drink';

const { expect, AssertionError } = chai;
let drinkService;

describe('DrinkService', () => {
  beforeEach(() => {
    drinkService = new DrinkService();
  });

  it('should create instnce', () => {
    expect(drinkService.drinkArray).to.be.deep.equal([]);
  });

  it('should call endpoint', async () => {
    drinkService = new DrinkService();
    nock('http://jakidrink.pl')
      .get('/drink/get/1')
      .reply(200, {
        id: 1,
        name: 'OK',
        created: 123456,
        description: 'desc',
        components: []
      });
    nock('http://jakidrink.pl')
      .get('/drink/get/2')
      .reply(200, {
        id: 1,
        name: 'OK',
        created: 123456,
        description: 'desc',
        components: []
      });
    nock('http://jakidrink.pl')
      .get('/drink/get/3')
      .reply(404);

    drinkService.getAll().then(result => {
      chai.expect(result).to.have.length(2);
      chai.expect(result).to.be.deep.equal([
        {
          id: 1,
          name: 'OK',
          created: 123456,
          description: 'desc',
          components: []
        },
        {
          id: 1,
          name: 'OK',
          created: 123456,
          description: 'desc',
          components: []
        }
      ]);
    });
  });
});
