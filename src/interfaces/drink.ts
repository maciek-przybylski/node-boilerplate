import { Component } from './component';

export interface Drink {
  id: number;
  name: string;
  created: number;
  description: string;
  components: Component[];
}
